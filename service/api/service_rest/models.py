from django.db import models

from django.urls import reverse

# Create your models here.
class Customer(models.Model):
    full_name = models.CharField(max_length=100)
    customer_id = models.PositiveIntegerField(primary_key= True)
    address = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=20, unique=True, null=False)
    href = models.CharField(max_length=50, unique=True, null=True, blank=True)

    def __str__(self):
        return f"{self.full_name}"



class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=50, unique=True, null=True)
    sold = models.BooleanField(default=False, null=True)


    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})


class EmployeeUserVO(models.Model):
    employee_id = models.PositiveIntegerField(unique=True)
    first_name = models.CharField(max_length=150, blank=True)
    last_name = models.CharField(max_length=150, blank=True)
    sale_employee = models.BooleanField(default=False)
    service_employee = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.first_name} {self.last_name} - {self.employee_id}"


class Technician(models.Model):
    name= models.CharField(max_length=50)
    employee_number= models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return f"{self.name}"

class Status(models.Model):

    id = models.PositiveSmallIntegerField(primary_key=True)
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)  # Default ordering for Status
        verbose_name_plural = "statuses"


class Service(models.Model):
    date = models.DateTimeField(auto_now=False, auto_now_add=True)
    time = models.TimeField(auto_now=False, auto_now_add=False, null=True)
    technician = models.ForeignKey(Technician, related_name = "tech", on_delete=models.CASCADE, null=True)
    reason = models.TextField()
    customer  = models.ForeignKey(Customer, related_name="customer", on_delete=models.CASCADE, null=True, blank=True)
    automobile = models.ForeignKey(AutomobileVO, related_name="auto", on_delete=models.CASCADE, null=True, blank=True)
    status = models.ForeignKey(
        Status,
        related_name="status",
        on_delete=models.PROTECT, null=True,
    )
    def finish(self):
        status = Status.objects.get(name="FINISHED")
        self.status = status
        self.save()

    def cancel(self):
        status = Status.objects.get(name="CANCELLED")
        self.status = status
        self.save()

    def __str__(self):
        return f"{self.reason}"

    @classmethod
    def create(cls, **kwargs):
        kwargs["status"] = Status.objects.get(name="PENDING")
        service = cls(**kwargs)
        service.save()
        return service
