from django.urls import path
from .api_views import api_list_service, api_show_service, api_list_customer, api_show_customer, api_list_technician, api_show_technician, api_finish_service_status, api_cancel_service_status


urlpatterns = [
    path("services/", api_list_service, name= "api_list_service"),
    path(
        "services/<int:pk>/",
        api_show_service,
        name="api_show_service"),
    path("customers/", api_list_customer, name= "api_list_customer"),
    path("customers/<int:pk>/", api_show_customer, name= "api_show_customer"),
    path("technicians/", api_list_technician, name= "api_list_technician"),
    path("technicians/<int:pk>/", api_show_technician, name= "api_show_technician"),
    path("services/<int:pk>/cancel/", api_cancel_service_status, name="api_cancel_service_status"),
    path("services/<int:pk>/finish/", api_finish_service_status, name="api_finish_service_status"),
]
