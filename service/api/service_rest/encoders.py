from common.json import ModelEncoder
from .models import AutomobileVO, Customer, Technician, Service

class AutomobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]
class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "href",
        "sold",
    ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "full_name",
        "customer_id",
    ]
class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "full_name",
        "customer_id",
        "address",
        "phone_number",

    ]

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
    ]
class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
    ]

class ServiceListEncoder(ModelEncoder):
    model = Service
    properties = [
        "id",
        "date",
        "technician",
        "reason",
        "customer",
        "automobile",
        "time",
        "status"
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
        "customer": CustomerListEncoder(),
        "automobile": AutomobileVOListEncoder(),
    }
    def get_extra_data(self, o):
        return {"status": o.status.name}
class ServiceDetailEncoder(ModelEncoder):
    model = Service
    properties = [
        "date",
        "technician",
        "reason",
        "customer",
        "automobile",
        "time",
        "status",
        "id"
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
        "customer": CustomerListEncoder(),
        "automobile": AutomobileVOListEncoder(),
    }
    def get_extra_data(self, o):
        return {"status": o.status.name}
