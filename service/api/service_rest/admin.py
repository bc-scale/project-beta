from django.contrib import admin
from .models import Service, Technician, Customer, Status, EmployeeUserVO

# Register your models here.
@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    pass

@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    pass

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass

@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    pass


@admin.register(EmployeeUserVO)
class EmployeeUserVO(admin.ModelAdmin):
    pass
