import React from "react";




class ServiceForm extends React.Component{
    state = {
            autos: [],
            automobile: "",
            customers: [],
            customer: "",
            date: "",
            time:"",
            technicians: [],
            technician:"",
            reason: "",
        }


    handleVinChange = (event) => {
        const value = event.target.value;
        this.setState({automobile: value})
    }
    handleCustomerChange = (event) => {
        const value = event.target.value;
        this.setState({customer: value})
    }
    handleDateChange = (event) => {
        const value = event.target.value;
        this.setState({date: value})
    }
    handleTimeChange = (event) => {
        const value = event.target.value;
        this.setState({time: value})
    }
    handleTechnicianChange = (event) => {
        const value = event.target.value;
        this.setState({technician: value})
    }
    handleReasonChange = (event) => {
        const value = event.target.value;
        this.setState({reason: value})
    }
    handleSubmit = async (event) => {
        event.preventDefault();
        const data= {...this.state};
        delete data.autos;
        delete data.technicians;
        delete data.customers;

        const serviceUrl = "http://localhost:8080/api/services/";
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {'Content-type': 'application/json'},
        };
        const response = await fetch(serviceUrl, fetchConfig);
        if (response.ok) {
            const newService = await response.json();

            const cleared = {
                automobile: "",
                customer: "",
                date: "",
                time:"",
                technician: "",
                reason: "",
            };
            this.setState(cleared);
          }

    }
    async componentDidMount() {
        const autoUrl = "http://localhost:8100/api/automobiles/";
        const autoResponse = await fetch(autoUrl);
        if( autoResponse.ok){
            const data = await autoResponse.json()
            this.setState({autos:data.autos})

        };

        const customerUrl = "http://localhost:8080/api/customers/";
        const customerResponse = await fetch(customerUrl);
        if( customerResponse.ok){
            const data = await customerResponse.json()
            this.setState({customers:data.customers})
        }

        const technicianUrl = "http://localhost:8080/api/technicians/";
        const technicianResponse = await fetch(technicianUrl);
        if( technicianResponse.ok){
            const data = await technicianResponse.json()
            this.setState({technicians:data.technicians})
        }

    }
    render(){
        return(
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new service</h1>
                <form onSubmit={this.handleSubmit} id="create-service-form">
                <div className="mb-3">
                    <select onChange={this.handleVinChange} value={this.state.auto} required name="vin" id="vin" className="form-select">
                      <option value="">Choose a vin</option>
                      {this.state.autos.map(auto =>{
                        return(
                          <option key={auto.vin} value={auto.vin}>
                            {auto.vin}
                          </option>
                        )
                      } )}
                    </select>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleCustomerChange} value={this.state.customer} required name="customer" id="customer" className="form-select">
                      <option value="">Choose a customer</option>
                      {this.state.customers.map(customer =>{
                        return(
                          <option key={customer.customer_id} value={customer.customer_id}>
                            {customer.full_name}
                          </option>
                        )
                      } )}
                    </select>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleDateChange} value={this.state.date} placeholder="date" required type="date" name="date" id="date" className="form-control"/>
                    <label htmlFor="date">Date</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleTimeChange} value={this.state.time} placeholder="time" required type="time" name="time" id="time" className="form-control"/>
                    <label htmlFor="time">Time</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleTechnicianChange} value={this.state.technician} required name="technician" id="technician" className="form-select">
                      <option value="">Choose a technician</option>
                      {this.state.technicians.map(technician =>{
                        return(
                          <option key={technician.employee_number} value={technician.employee_number}>
                            {technician.name}
                          </option>
                        )
                      } )}
                    </select>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleReasonChange} value={this.state.reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control"/>
                    <label htmlFor="reason">Reason</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
}


export default ServiceForm;
