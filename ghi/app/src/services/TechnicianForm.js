import React from "react";




class TechnicianForm extends React.Component{
    state= {
      name: "",
      employee_number : "",
    }


  handleNameChange = (event) => {
    const value = event.target.value;
    this.setState({name: value})
  }
  handleEmployeeNumberChange = (event) => {
    const value = event.target.value;
    this.setState({employee_number: value})
  }
  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {...this.state};


    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {'Content-type': 'application/json'},
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      const cleared = {
        name: '',
        employee_number: '',
      };
      this.setState(cleared);
    }
    else {
      const data = await response.json();
      alert(data.message)
    }
  }

    render(){
        return(
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new Technician</h1>
                <form onSubmit={this.handleSubmit} id="create-customer-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEmployeeNumberChange} value={this.state.employee_number} placeholder="employeenumber" required type="number" name="employee_number" id="employee_number" className="form-control"/>
                    <label htmlFor="employee_number">Employee Number</label>
                  </div>
                  <div className="mb-3">
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
}



export default TechnicianForm;
