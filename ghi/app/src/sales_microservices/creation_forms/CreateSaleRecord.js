import React from "react";


class SaleRecordForm extends React.Component{
  state ={
    automobiles: [],
    automobile: "",
    
    employee_users: [],
    employee_user: "",
    
    customers: [],
    customer: "",
    
    sales_price: '',
    };
  
  handleSubmit = async (e) => {
    e.preventDefault();
    const data ={...this.state};
    delete data.automobiles
    delete data.employee_users
    delete data.customers

    const salesRecordUrl = "http://localhost:8090/api/sales-records/"
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(salesRecordUrl, fetchConfig)
    if (response.ok) {
      const newSaleRecord = await response.json();

      const cleared ={
        automobile: '',
        employee_user: '',
        customer: '',
        sales_price: '',
      };

      this.setState(cleared)
    }
    window.location.href = `http://localhost:3000/list/sales/`
  }

  handleAutomobileChange = (e) => {
    const value= e.target.value;
    this.setState({automobile: value})
  }
  handleSalesPersonChange = (e) =>{
    const value= e.target.value;
    this.setState({employee_user: value})
  }
  handleCustomerChange = (e) =>{
    const value= e.target.value;
    this.setState({customer: value})
  }
  handleSalesPriceChange = (e) =>{
    const value= e.target.value;
    this.setState({sales_price: value})
  }

  componentDidMount = async (props) => {

    const automobilesUrl = 'http://localhost:8090/api/automobiles/';
    const automobilesUrlResponse = await fetch(automobilesUrl);
    if (automobilesUrlResponse.ok) {
      const data = await automobilesUrlResponse.json();
      this.setState({automobiles:data.automobiles});
    }

    const salesPersonsUrl = 'http://localhost:8090/api/employees/';
    const salesPersonsUrlResponse = await fetch(salesPersonsUrl);
    if (salesPersonsUrlResponse.ok) {
      const data = await salesPersonsUrlResponse.json();
      this.setState({employee_users:data.employee_users});
    }

    const customersUrl = 'http://localhost:8090/api/customers/';
    const customersUrlResponse = await fetch(customersUrl);
    if (customersUrlResponse.ok) {
        const data = await customersUrlResponse.json();
        this.setState({customers:data.customers});
    }
  }

    render(){
      return(
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Record a new sale</h1>
              <form onSubmit={this.handleSubmit} id="create-customer-form">
                <select onChange={this.handleAutomobileChange} required name="automobile" id="automobile" className="form-select">
                  <option value="">Choose a automobile</option>
                    { this.state.automobiles.map(automobile => {
                    if (automobile.sold === true){
                      return(null)
                    }else{
                      return (<option key={automobile.vin} value={automobile.vin}>{automobile.model} - {automobile.vin}</option>
                      )}
                  })} 
                </select>
                <br/>
                <select onChange={this.handleSalesPersonChange} required name="employee_user" id="employee_user" className="form-select">
                  <option value="">Choose a employee</option>
                    { this.state.employee_users.map(employee => {                      if (employee.service_employee === true){
                        return(null)
                      }else{

                      return (
                    <option key={employee.employee_id} value={employee.employee_id}>{employee.first_name} {employee.last_name}</option>
                    )
                      }
                  })}
                </select>
                <br/>
                <select onChange={this.handleCustomerChange} required name="customer" id="customer" className="form-select">
                  <option value="">Choose a customer</option>
                    { this.state.customers.map(customer => {
                      return (<option key={customer.id} value={customer.id}>{customer.full_name}</option>
                      )
                  })}
                </select>
                <br/>
                <div className="form-floating mb-3">
                  <input onChange={this.handleSalesPriceChange} placeholder="sales_price" required type="number" name="sales_price" id="sales_price" className="form-control" min="0" />
                  <label htmlFor="sales_price">Sale price</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
    }



export default SaleRecordForm;
