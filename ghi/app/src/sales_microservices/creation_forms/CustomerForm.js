import React from "react";


class CustomerForm extends React.Component{
  state ={full_name: '',phone_number: '',address: ''};


  handleFullNameChange = (e) => {
    const value= e.target.value;
    this.setState({full_name: value})
  }
  handlePhoneNumberChange = (e) => {
    const value= e.target.value;
    this.setState({phone_number: value})
  }
  handleAddressChange = (e) => {
    const value= e.target.value;
    this.setState({address: value})
  }
  
  handleSubmit = async (e) => {
    e.preventDefault();
    const data ={...this.state};



    const customersUrl = "http://localhost:8090/api/customers/"
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(customersUrl, fetchConfig)
    if (response.ok) {
      const newCustomer = await response.json();

      const cleared ={
        full_name: '',
        phone_number: '',
        address: '',

      };
      this.setState(cleared)
    }
  }


    render(){
        return(
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create customer</h1>
                <form onSubmit={this.handleSubmit} id="create-customer-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFullNameChange} value={this.state.full_name} placeholder="full_name" required type="text" name="full_name" id="full_name" className="form-control"/>
                    <label htmlFor="full_name">Full Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePhoneNumberChange} value={this.state.phone_number} placeholder="Style name" required type="text" name="phone_number" id="phone_number" className="form-control"/>
                    <label htmlFor="phone_number">Phone Number</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleAddressChange} value={this.state.address} placeholder="Color" required type="text" name="address" id="address" className="form-control"/>
                    <label htmlFor="address">Address</label>
                  </div>
                <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }



export default CustomerForm;
