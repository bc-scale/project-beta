import React from "react";


class AutomobileForm extends React.Component{
  state ={
    color: '',
    year: '',
    vin: '',
    models: [],
    model_id: '',
    };

  handleColorChange = (event) => {
    const value= event.target.value;
    this.setState({color: value})
  }
  handleYearChange = (event) => {
    const value= event.target.value;
    this.setState({year: value})
  }
  handleVinChange = (event) => {
    const value= event.target.value;
    this.setState({vin: value})
  }
  handleModelChange = (event) => {
    const value= event.target.value;
    this.setState({model_id: value})
  }

  handleSubmit = async (event) => {
      event.preventDefault();
      const data ={...this.state};
      delete data.models

      const url = "http://localhost:8100/api/automobiles/";
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newAutomobile = await response.json();

            const cleared ={models: ''};
            this.setState(cleared)
    }
    window.location.href = `http://localhost:3000/list/automobiles/`
  }
  componentDidMount = async (props) => {

    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({models:data.models});
    }
  }


    render(){
        return(
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add an automobile to inventory</h1>
                <form onSubmit={this.handleSubmit} id="create-automobile-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleColorChange} value={this.state.color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleYearChange} value={this.state.year} placeholder="year" required type="text" name="year" id="year" className="form-control"/>
                    <label htmlFor="year">Year</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleVinChange} value={this.state.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                    <label htmlFor="vin">Vin</label>
                  </div>
                  <select onChange={this.handleModelChange} required name="model" id="model" className="form-select">
                    <option value="">Choose a model</option>
                    { this.state.models.map(model => {
                        return (
                            <option key={model.id} value={model.id}>{model.name}</option>
                        )
                    })}
                  </select>
                  <br/>
                <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }



export default AutomobileForm;
