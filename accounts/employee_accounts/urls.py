from django.urls import path

from .views import (
    list_accounts,
    detail_employee,
    )

urlpatterns = [
    #Get, Post
    path("", list_accounts, name="list_accounts"),
    #Get, Delete, Update
    path("<int:employee_id>/", detail_employee, name="del_get_up_accounts"), 
]
