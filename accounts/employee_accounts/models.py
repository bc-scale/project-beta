from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class User(AbstractUser):
    employee_id = models.PositiveSmallIntegerField(unique=True, null=True)
    sale_employee = models.BooleanField(default=False)
    service_employee = models.BooleanField(default=False)
    
