from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    href = models.CharField(max_length=250)
    sold = models.BooleanField(default=False)
    model = models.CharField(max_length=15)

    def __str__(self):
        return f"{self.model}"


class EmployeeUserVO(models.Model):
    employee_id = models.PositiveIntegerField(unique=True, null=False)
    first_name = models.CharField(max_length=150, blank=True)
    last_name = models.CharField(max_length=150, blank=True)
    sale_employee = models.BooleanField(default=False)
    service_employee = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.first_name} {self.last_name} - {self.employee_id}"


class SalePerson(models.Model):
    full_name = models.CharField(max_length=250)
    employee_id = models.PositiveIntegerField(unique=True, )

    def __str__(self):
        return f"{self.full_name} - {self.employee_id}"

class Customer(models.Model):
    full_name = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=20, null=False)
    address = models.CharField(max_length=250)

    def __str__(self):
        return f"{self.full_name} - {self.phone_number}"


class SalesRecord(models.Model):
    employee_user = models.ForeignKey(EmployeeUserVO, related_name="employee_user", on_delete=models.PROTECT)
    automobile = models.ForeignKey(AutomobileVO, related_name="automobilevo_vin", on_delete=models.CASCADE)
    customer = models.ForeignKey("Customer", related_name="customer", on_delete=models.PROTECT)
    sales_price = models.PositiveIntegerField()

    def __str__(self):
        return f"{self.automobile} - {self.employee_user}, {self.customer}"
